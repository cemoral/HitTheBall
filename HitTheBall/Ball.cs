using System;
using Android.Graphics;
using Java.Lang;
using System.Threading.Tasks;
using Thread = System.Threading.Thread;
using HitTheBall.Fragments;

namespace HitTheBall
{
    public class Ball
    {
        public int X { get; set; }
        public int Y { get; set; }
        public float Radius { get; set; }
        public Color C { get; set; }
        public bool Increment;
        public int GreenStack;
        public int BlueStack;
        private Random rnd = new Random();
     
        private int _width;
        private int _height;
        private int _maxRadius;
        private Thread colorThread;
        public Ball(int Width, int Height, int MaxRadius)
        {
            _width = Width;
            _height = Height;
            _maxRadius = MaxRadius;
            colorThread = new Thread(() => ChangeColor(1000));
            Draw();
        }
        public void Draw()
        {
            X = rnd.Next(_maxRadius, _width - _maxRadius);
            Y = rnd.Next(_maxRadius, _width - _maxRadius);
            Radius = 0;
            Increment = true;
            if (colorThread.ThreadState == System.Threading.ThreadState.Unstarted)
                colorThread.Start();
            if (colorThread.ThreadState == (System.Threading.ThreadState.AbortRequested | System.Threading.ThreadState.Stopped) || colorThread.ThreadState == System.Threading.ThreadState.Stopped)
            {
                colorThread = new Thread(() => ChangeColor(1000));
                colorThread.Start();
            }

        }

        public void ChangeColor(int CSpeed)
        {

            while (GameFragment.game.CurrentState == Game.GameState.Running)
            {
                int x = rnd.Next(3);
                switch (x)
                {
                    case 0:
                        C = new Color(0, 0, 255);
                        break;
                    case 1:
                        C = new Color(0, 255, 0);
                        break;
                    case 2:
                        C = new Color(255, 0, 0);
                        break;
                }
                Thread.Sleep(CSpeed);
            }
        }
        public void Run(float rSpeed)
        {
            if (Increment)
            {
                Radius += rSpeed;
                if (Radius >= _maxRadius)
                    Increment = false;
            }
            else if (!Increment)
            {
                Radius -= rSpeed;
                if (Radius <= 0)
                {
                    Increment = true;
                    GameFragment.game.STAGE_SCORE -= GameFragment.game.GAME_STAGE;
                    GreenStack = BlueStack = 0;
                    GameFragment.game.ClickedBallColor = Color.Red;
                    Draw();
                    GameFragment.game.IsChanged = true;
                }
            }

        }
    }
}