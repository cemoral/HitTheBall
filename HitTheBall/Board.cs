using System;
using System.Threading;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using static Android.Graphics.Paint;
using Android.Animation;
using HitTheBall.Fragments;

namespace HitTheBall
{
    public class Board : View
    {
        public new static int Height;
        private Paint pLeft = new Paint();
        private Paint pRight = new Paint();
        private Paint pScore = new Paint();
        private ValueAnimator va;

        public Board(Context context) : base(context)
        {
            pScore.Color = pRight.Color = pLeft.Color = Color.White;
            pRight.TextSize = pLeft.TextSize = TypedValue.ApplyDimension(ComplexUnitType.Dip, 16, Resources.DisplayMetrics);
            pScore.TextSize = TypedValue.ApplyDimension(ComplexUnitType.Dip, 20, Resources.DisplayMetrics);
            pRight.TextAlign = Align.Right;
            pLeft.SetTypeface(Typeface.DefaultBold);
            pRight.SetTypeface(Typeface.DefaultBold);
            pScore.SetTypeface(Typeface.DefaultBold);
            pScore.TextAlign = Align.Center;

        }
        protected override void OnDraw(Canvas canvas)
        {
            if (Height == 0)
                Height = canvas.Height;
            try
            {
                switch (GameFragment.game.CurrentState)
                {
                    case Game.GameState.Ready:
                        break;
                    case Game.GameState.Running:
                        canvas.DrawText("BEST : " + GameFragment.game.BEST_SCORE, 0, pLeft.TextSize*2, pLeft);
                        canvas.DrawText(GameFragment.game.STAGE_SCORE.ToString(), canvas.Width / 2, canvas.Height, pScore);
                        break;
                    case Game.GameState.Paused:
                        canvas.DrawText("BEST : " + GameFragment.game.BEST_SCORE, 0, pLeft.TextSize*2, pLeft);
                        canvas.DrawText(GameFragment.game.STAGE_SCORE.ToString(), canvas.Width / 2, canvas.Height, pScore);
                        break;
                    case Game.GameState.Dead:
                        canvas.DrawText("BEST : " + GameFragment.game.BEST_SCORE, 0, pLeft.TextSize*2, pLeft);
                        break;
                }

                canvas.DrawText("STAGE : " + GameFragment.game.GAME_STAGE, canvas.Width, pRight.TextSize*2, pRight);
                if (GameFragment.game.IsChanged)
                {
                    // TextAnimation(Ball.C,pScore);
                    TextAnimation(GameFragment.game.ClickedBallColor, pScore);
                    GameFragment.game.IsChanged = false;
                }
                if (GameFragment.game.IsStageUpped)
                {
                    TextAnimation(Color.Green, pRight);
                    GameFragment.game.IsStageUpped = false;
                }
                Invalidate();
                Thread.Sleep(16);
            }
            catch (Exception)
            {

            }

        }



        private void TextAnimation(Color C, Paint p)
        {
            int colorStart = C;
            int colorMid = Color.White;

            va = ObjectAnimator.OfInt(p, "Color", colorStart, colorMid);
            va.SetDuration(500);
            va.SetEvaluator(new ArgbEvaluator());

            va.RepeatCount = 0;
            va.RepeatMode = ValueAnimatorRepeatMode.Restart;
            va.Start();
        }


    }
}