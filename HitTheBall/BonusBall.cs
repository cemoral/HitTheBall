using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;
using static HitTheBall.Game;
using Java.Lang;
using Thread = System.Threading.Thread;
using HitTheBall.Fragments;

namespace HitTheBall
{
    class BonusBall
    {
        public int X { get; set; }
        public int Y { get; set; }
        public int Radius { get; set; }
        public Color C { get; set; }
        private bool _artis = true;
        private int MAX_RADIUS;
        private Paint p;
        private Canvas canvas;
        private Thread tBonusBall;
        private Thread tBonusBallRenk;
        
        private Random rnd = new Random();
        public BonusBall(Canvas mCanvas,float maxR)
        {
            X = rnd.Next(100, mCanvas.Width - 100);
            Y = rnd.Next(100, mCanvas.Height - 100);
            Radius = 0;
            canvas = mCanvas;
            p = new Paint();
            tBonusBall = new Thread(Start);
            tBonusBallRenk = new Thread(RenkDegistir);
            
        }
        public void Start()
        {
            if (tBonusBall.ThreadState == System.Threading.ThreadState.Unstarted)
            {
                tBonusBall.Start();
                tBonusBallRenk.Start();
                return;
            }
            //if (tBonusBallRenk.ThreadState == System.Threading.ThreadState.Unstarted)
             


            if (_artis)
            {
                Radius += 1;
                if (Radius >= MAX_RADIUS)
                    _artis = false;
            }
            else if (!_artis)
            {
                Radius -= 1;
                if (Radius <= 0)                          
                    Stop();           
            }

            canvas.DrawCircle(X, Y, Radius, p);
            Thread.Sleep(16);
        }
        private void Stop()
        {
            tBonusBall.Abort();
            tBonusBallRenk.Abort();

        }
        private void RenkDegistir()
        {
            while (GameFragment.game.CurrentState == GameState.Running)
            {
                int x = rnd.Next(3);
                switch (x)
                {
                    case 0:
                        C = new Color(0, 0, 255);
                        break;
                    case 1:
                        C = new Color(0, 255, 0);
                        break;
                    case 2:
                        C = new Color(255, 0, 0);
                        break;
                }
                p.Color = C;
                Thread.Sleep(16);
            }
        }

    }
}