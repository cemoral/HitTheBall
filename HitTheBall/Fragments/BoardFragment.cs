using Android.OS;
using Android.Views;
using Android.Support.V4.App;

namespace HitTheBall.Fragments
{
    public class BoardFragment : Fragment
    {
        Board board;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            
            board = new Board(Context);
            // Create your fragment here
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {            
            // Use this to return your custom view for this Fragment
            // return inflater.Inflate(Resource.Layout.YourFragment, container, false);
           // View view = inflater.Inflate(Resource.Layout.Board, container, false);
            return board;
        }
    }
}