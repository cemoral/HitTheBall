using Android.OS;
using Android.Views;
using Android.Support.V4.App;


namespace HitTheBall.Fragments
{
    class GameFragment : Fragment
    {
        public static Game game;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            game = new Game(Context);
           
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            game.CurrentState = Game.GameState.Ready;
            game.ReadyTask();         
            return game;
        }
    }
}