using System;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Util;
using Android.Views;
using Java.IO;
using Color = Android.Graphics.Color;
using Exception = System.Exception;
using Math = System.Math;
using Thread = System.Threading.Thread;
using File = Java.IO.File;
using Random = System.Random;
using Android.Graphics.Drawables;
using Android.Media;
using Path = Android.Graphics.Path;
using System.Threading.Tasks;
using Android.App;
using System.Collections.Generic;
using Android.Widget;

namespace HitTheBall
{
    public class Game : View
    {

        public enum GameState
        {
            Running,
            Paused,
            Ready,
            Dead
        }

        public static bool IsVibrateOn = true;
        public static bool IsSoundOn = true;
        public Color ClickedBallColor;
        public int BEST_SCORE;
        public int STAGE_SCORE;
        public int TOTAL_SCORE;
        private float R_SPEED;
        private bool FIRST_RUN;
        public int GAME_STAGE;
        private int C_SPEED;
        private int GREEN_POINT = 10;
        private int BLUE_POINT = 5;
        private int RED_POINT = -10;
        private float MAX_RADIUS;


        private StorageService storageService;
        private Canvas mCanvas;
        private Paint p = new Paint();
        private Paint p1 = new Paint();
        private Paint p2 = new Paint();
        private Paint p3 = new Paint();

        private Task pausedTask;

        public GameState CurrentState = GameState.Ready;
        Random rnd = new Random();
        private bool _isPaused = false;
        private int _cooldown;
        private MediaPlayer _player;
        private Vibrator _vib;
        private Path _comboPath;
        public bool IsChanged = false;
        public bool IsStageUpped = false;
        private Ball ball;

        private List<Bitmap> expGreen = new List<Bitmap>();
        private List<Bitmap> expBlue = new List<Bitmap>();
        private List<Bitmap> expRed = new List<Bitmap>();

        public Game(Context context) : base(context)
        {            
            _vib = (Vibrator)context.GetSystemService(Context.VibratorService);
            p2.Color = p1.Color = Color.White;
            p3.TextSize = p1.TextSize = TypedValue.ApplyDimension(ComplexUnitType.Dip, 30, Resources.DisplayMetrics);
            p2.TextSize = TypedValue.ApplyDimension(ComplexUnitType.Dip, 40, Resources.DisplayMetrics);
            p.TextAlign = p1.TextAlign = p3.TextAlign = p2.TextAlign = Paint.Align.Center;
            _player = MediaPlayer.Create(Application.Context, Resource.Raw.bomb);
            _comboPath = new Path();

            Thread tAnim = new Thread(() => LoadAnimations());
            tAnim.Start();            

            storageService = new StorageService(context.FilesDir);
            BEST_SCORE = Convert.ToInt32(storageService.ReadSettings().Score);            
            
        }
        int i = 0;
        int rr;

        char IsClicked = 'x';
        protected override void OnDraw(Canvas canvas)
        {
            base.OnDraw(canvas);
            try
            {
                if (mCanvas == null)
                {
                    mCanvas = canvas;
                    ball = new Ball(canvas.Width, canvas.Height, Convert.ToInt32(MAX_RADIUS));
                }

                switch (CurrentState)
                {
                    case GameState.Ready:
                        ReadyTask();
                        break;
                    case GameState.Running:                       
                        if (_isPaused)
                        {
                            pausedTask = new Task(() => CoolDown(canvas));
                            Thread.Sleep(1000);
                            pausedTask.Start();
                        }
                        else
                        {
                            if (IsClicked == 'x')
                            {
                                if (ball.BlueStack + ball.GreenStack >= 2)
                                    canvas.DrawTextOnPath(ball.GreenStack + ball.BlueStack + "x", _comboPath, 0, 0, p3);
                                canvas.DrawCircle(ball.X, ball.Y, ball.Radius, p);
                            }
                            else
                            {
                                Bitmap rBmp;
                                if (IsClicked == 'g')
                                {
                                    rBmp = Bitmap.CreateScaledBitmap(expGreen[i], (int)rr, (int)rr, false);
                                }
                                else if (IsClicked == 'b')
                                {
                                    rBmp = Bitmap.CreateScaledBitmap(expBlue[i], (int)rr, (int)rr, false);
                                }
                                else
                                {
                                    rBmp = Bitmap.CreateScaledBitmap(expRed[i], (int)rr, (int)rr, false);
                                }
                                canvas.DrawBitmap(rBmp, ball.X-rr/2, ball.Y-rr/2, p);
                                i++;
                                if (i == expGreen.Count)
                                {
                                    i = 0;
                                    IsClicked = 'x';
                                    rBmp.Recycle();
                                    reDraw(ball);
                                }
                            }
                        }
                        RunningTask();
                        break;
                    case GameState.Paused:
                        canvas.DrawText("GAME PAUSED", (canvas.Width) / 2, canvas.Height / 2, p2);
                        PausedTask();
                        break;
                    case GameState.Dead:
                        canvas.DrawText("GAME OVER", (canvas.Width) / 2, canvas.Height / 2, p2);
                        canvas.DrawText("Press To Restart", mCanvas.Width / 2, canvas.Height / 2 + p1.TextSize, p1);
                        DeadTask();
                        break;
                }
            }
            catch (Exception ex)
            {

            }
        }

        void CoolDown(Canvas canvas)
        {
            canvas.DrawText("GAME STARTS IN", canvas.Width / 2, canvas.Height / 2, p2);
            canvas.DrawText(_cooldown--.ToString(), mCanvas.Width / 2, canvas.Height / 2 + p1.TextSize, p1);
            if (_cooldown == 0)
                _isPaused = false;
        }

        public void reDraw(Ball b)
        {
            b.Draw();
            if (!FIRST_RUN)
                IsChanged = true;
        }

        private void StageUp()
        {
            C_SPEED -= 100;
            GAME_STAGE++;
            STAGE_SCORE = 0;
            R_SPEED += 0.3f;
            IsStageUpped = true;
        }

        public void ReadyTask()
        {
            FIRST_RUN = true;
            GAME_STAGE = 1;
            C_SPEED = 1500;
            R_SPEED = 1;
            IsClicked = 'x';
            STAGE_SCORE = 0;
            TOTAL_SCORE = 0;
            Focusable = true;
            MAX_RADIUS = TypedValue.ApplyDimension(ComplexUnitType.Dip, 40, Resources.DisplayMetrics);
            CurrentState = GameState.Running;
            RunningTask();
        }

        public void RunningTask()
        {
            if (ball == null)
                return;

            try
            {
                if (FIRST_RUN)
                {
                    reDraw(ball);
                    FIRST_RUN = false;
                }

                ball.Run(R_SPEED);

                if (TOTAL_SCORE >= BEST_SCORE)
                    BEST_SCORE = TOTAL_SCORE;

                if (STAGE_SCORE >= 100)
                    StageUp();

                if (STAGE_SCORE < 0)
                    CurrentState = GameState.Dead;

                if (ball.GreenStack > 0)
                    p3.Color = Color.Green;
                else
                    p3.Color = Color.Blue;

                _comboPath.Reset();
                _comboPath.AddArc(new RectF(ball.X - ball.Radius, ball.Y - ball.Radius, ball.X, ball.Y), 200, 40);

                p.Color = ball.C;

                Invalidate();
                Thread.Sleep(16);
            }

            catch (Exception ex)
            {

            }
        }

        public void PausedTask()
        {
            _isPaused = true;
            _cooldown = 3;
        }

        private void DeadTask()
        {
            storageService.SaveSettings(BEST_SCORE.ToString(),IsSoundOn,IsVibrateOn);
        }      

        public override bool OnTouchEvent(MotionEvent e)
        {
            if (e.Action == MotionEventActions.Down)
                switch (CurrentState)
                {
                    case GameState.Ready:

                        break;
                    case GameState.Running:
                        try
                        {
                            double y2 = Math.Pow((e.RawY - Board.Height - ball.Y), 2);
                            double x2 = Math.Pow((e.RawX - ball.X), 2);

                            if ((Math.Sqrt(y2 + x2) <= ball.Radius))
                            {
                                ClickedBallColor = ball.C;

                                if (ball.C == Color.Red)
                                {
                                    STAGE_SCORE += RED_POINT;
                                    ball.BlueStack = 0;
                                    ball.GreenStack = 0;
                                    IsClicked = 'r';

                                }
                                else if (ball.C == Color.Blue)
                                {

                                    ball.BlueStack++;
                                    ball.GreenStack = 0;
                                    TOTAL_SCORE += BLUE_POINT + (ball.BlueStack > 1 ? (ball.BlueStack * 2) : 0);
                                    STAGE_SCORE += BLUE_POINT + (ball.BlueStack > 1 ? (ball.BlueStack * 2) : 0);
                                    IsClicked = 'b';
                                }
                                else if (ball.C == Color.Green)
                                {

                                    ball.GreenStack++;
                                    ball.BlueStack = 0;
                                    TOTAL_SCORE += GREEN_POINT + (ball.GreenStack > 1 ? (ball.GreenStack * 2) : 0);
                                    STAGE_SCORE += GREEN_POINT + (ball.GreenStack > 1 ? (ball.GreenStack * 2) : 0);
                                    IsClicked = 'g';
                                }
                                if (IsSoundOn)
                                    if (_player.IsPlaying)
                                        _player.SeekTo(0);
                                    else
                                        _player.Start();
                                                       
                                rr = (int)ball.Radius;
                                if (IsVibrateOn)
                                    _vib.Vibrate(25);
                            }
                        }
                        catch (Exception ex)
                        {

                        }
                        break;
                    case GameState.Dead:
                        break;
                    case GameState.Paused:
                        break;
                }

            return true;
        }
        private void LoadAnimations()
        {
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_1));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_2));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_3));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_4));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_5));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_6));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_7));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_8));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_9));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_10));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_11));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_12));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_13));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_14));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_15));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_16));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_18));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_19));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_20));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_21));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_22));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_23));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_24));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_25));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_26));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_27));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_28));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_29));
            expGreen.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.gframe_30));

            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_1));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_2));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_3));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_4));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_5));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_6));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_7));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_8));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_9));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_10));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_11));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_12));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_13));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_14));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_15));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_16));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_18));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_19));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_20));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_21));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_22));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_23));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_24));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_25));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_26));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_27));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_28));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_29));
            expBlue.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.bframe_30));

            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_1));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_2));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_3));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_4));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_5));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_6));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_7));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_8));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_9));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_10));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_11));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_12));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_13));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_14));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_15));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_16));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_18));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_19));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_20));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_21));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_22));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_23));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_24));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_25));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_26));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_27));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_28));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_29));
            expRed.Add(BitmapFactory.DecodeResource(Resources, Resource.Drawable.rframe_30));
        }
    }
}