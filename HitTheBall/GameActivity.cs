﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using HitTheBall.Fragments;
using Android.Support.V7.App;

namespace HitTheBall
{
    [Activity(Label = "GameActivity", Theme = "@style/Theme.AppCompat.Light.NoActionBar.FullScreen")]
    public class GameActivity : AppCompatActivity
    {
        public static ImageButton ibtnPause;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            var boardFragment = new BoardFragment();
            var gameFragment = new GameFragment();
            var trans = SupportFragmentManager.BeginTransaction();
            trans.Add(Resource.Id.BoardFragmentContainer, boardFragment, "BoardFragment");
            trans.Add(Resource.Id.GameFragmentContainer, gameFragment, "GameFragment");
            trans.Commit();
            SetContentView(Resource.Layout.InGame);
            ibtnPause = FindViewById<ImageButton>(Resource.Id.iBtnPause);
            ibtnPause.SetBackgroundResource(Resource.Drawable.pause);
            ibtnPause.Click += ibtnPause_Click;
        }
        public override void OnBackPressed()
        {
            if (GameFragment.game != null)
            {
                if (GameFragment.game.CurrentState == Game.GameState.Running)
                {
                    Toast.MakeText(GameFragment.game.Context, "Press Back To Exit", ToastLength.Short).Show();
                    ibtnPause.CallOnClick();
                }
                else
                {
                    new StorageService(FilesDir).SaveSettings(GameFragment.game.BEST_SCORE.ToString(),Game.IsSoundOn,Game.IsVibrateOn);
                    base.OnBackPressed();
                }
            }
            else
                base.OnBackPressed();
        }
        private void ibtnStop_Click(object sender, EventArgs e)
        {
            if (GameFragment.game.CurrentState == Game.GameState.Paused)
            {
                base.OnBackPressed();
            }
        }       
        private void ibtnPause_Click(object sender, EventArgs e)
        {
            switch (GameFragment.game.CurrentState)
            {
                case Game.GameState.Running:
                    GameFragment.game.CurrentState = Game.GameState.Paused;
                    GameFragment.game.PausedTask();
                    ibtnPause.SetBackgroundResource(Resource.Drawable.play);
                    break;
                case Game.GameState.Paused:
                    GameFragment.game.CurrentState = Game.GameState.Running;
                    GameFragment.game.RunningTask();
                    ibtnPause.SetBackgroundResource(Resource.Drawable.pause);
                    break;
                case Game.GameState.Dead:
                    GameFragment.game.CurrentState = Game.GameState.Ready;
                    GameFragment.game.ReadyTask();
                    ibtnPause.SetBackgroundResource(Resource.Drawable.play);
                    break;
            }
        }
    }
}