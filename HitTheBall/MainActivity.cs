﻿using System;
using Android.App;
using Android.Graphics;
using Android.Widget;
using Android.OS;
using HitTheBall.Fragments;
using Android.Views.Animations;
using Android.Graphics.Drawables;
using Android.Animation;
using Android.Support.V7.App;
using Android.Content;

namespace HitTheBall
{
    [Activity(Label = "HitTheBall", Icon = "@drawable/icon", Theme = "@style/Theme.AppCompat.Light.NoActionBar.FullScreen")]
    public class MainActivity : AppCompatActivity
    {
        private Button btnStart;
        private Button btnSoundSwitch;
        private Button btnHelp;
        private Button btnVibrateSwitch;
        private StorageService storageService;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.main);
            storageService = new StorageService(FilesDir);
            Game.IsSoundOn = storageService.ReadSettings().SoundValue;
            Game.IsVibrateOn = storageService.ReadSettings().VibrationValue;
            InitializeComponent();
        }
        protected override void OnResume()
        {
            base.OnResume();
        }

        private void BtnHelp_Click(object sender, EventArgs e)
        {
            StartActivity(new Intent(Application.Context, typeof(HelpActivity)));
        }

        private void BtnSoundSwitch_Click(object sender, EventArgs e)
        {
            if (Game.IsSoundOn)
            {
                btnSoundSwitch.SetBackgroundResource(Resource.Drawable.soundOff);
                Game.IsSoundOn = false;
            }
            else
            {
                btnSoundSwitch.SetBackgroundResource(Resource.Drawable.soundOn);
                Game.IsSoundOn = true;
            }
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            StartActivity(new Intent(Application.Context, typeof(GameActivity)));
        }

        private void BtnVibrateSwitch_Click(object sender, EventArgs e)
        {
            if (Game.IsVibrateOn)
            {
                btnVibrateSwitch.SetBackgroundResource(Resource.Drawable.vibOff);
                Game.IsVibrateOn = false;
            }
            else
            {
                btnVibrateSwitch.SetBackgroundResource(Resource.Drawable.vibOn);
                Game.IsVibrateOn = true;
            }
        }

        public override void OnBackPressed()
        {
            if (GameFragment.game != null)
                new StorageService(FilesDir).SaveSettings(GameFragment.game.BEST_SCORE.ToString(), Game.IsSoundOn, Game.IsVibrateOn);
            else
                new StorageService(FilesDir).SaveSettings(storageService.ReadSettings().Score, Game.IsSoundOn, Game.IsVibrateOn);
            base.OnBackPressed();
        }

        private void TextAnimation()
        {
            int colorStart = Color.Green;
            int colorMid = Color.Blue;
            int colorEnd = Color.Red;
            TextView tw = FindViewById<TextView>(Resource.Id.txtSplash);
            ValueAnimator va = ObjectAnimator.OfInt(tw, "textColor", colorStart, colorMid, colorEnd);
            va.SetDuration(2000);
            va.SetEvaluator(new ArgbEvaluator());
            va.RepeatCount = Animation.Infinite;
            va.RepeatMode = ValueAnimatorRepeatMode.Reverse;
            va.Start();
        }

        private void InitializeComponent()
        {
            btnStart = FindViewById<Button>(Resource.Id.btnStart);
            btnStart.SetTextColor(Color.White);
            btnStart.Click += BtnStart_Click;
            btnSoundSwitch = FindViewById<Button>(Resource.Id.btnSound);
            btnSoundSwitch.Click += BtnSoundSwitch_Click;
            btnHelp = FindViewById<Button>(Resource.Id.btnHelp);
            btnHelp.Click += BtnHelp_Click;
            btnVibrateSwitch = FindViewById<Button>(Resource.Id.btnVibrate);
            btnVibrateSwitch.Click += BtnVibrateSwitch_Click;
            TextAnimation();

            if (Game.IsSoundOn)
                btnSoundSwitch.SetBackgroundResource(Resource.Drawable.soundOn);
            else
                btnSoundSwitch.SetBackgroundResource(Resource.Drawable.soundOff);

            if (Game.IsVibrateOn)
                btnVibrateSwitch.SetBackgroundResource(Resource.Drawable.vibOn);
            else
                btnVibrateSwitch.SetBackgroundResource(Resource.Drawable.vibOff);
        }
    }
}

