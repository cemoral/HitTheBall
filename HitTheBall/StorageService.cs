﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.IO;

namespace HitTheBall
{
    class StorageService
    {
        File file;
        public string Score;
        public bool VibrationValue;
        public bool SoundValue;

        public StorageService(File filesDir)
        {
            file = new File(filesDir, @"stt.dat");
        }

        public StorageService ReadSettings()
        {
            try
            {
                if (!file.Exists())
                {
                    file.CreateNewFile();
                    SaveSettings("0", true, true);
                }

                FileReader fr = new FileReader(file);
                char[] s = new char[20];
                fr.Read(s);
                string sc = "";
                foreach (char c in s)
                {
                    if (c == '\0')
                        break;

                    sc += c;
                }
                Score = sc.Split('.')[0];
                SoundValue = Convert.ToBoolean(sc.Split('.')[1]);
                VibrationValue = Convert.ToBoolean(sc.Split('.')[2]);
                return this;
            }
            catch (Exception e)
            {
                return this;
            }
        }
        public void SaveSettings(string score, bool sound, bool vib)
        {
            FileWriter fw = new FileWriter(file);
            fw.Write(score + "." + sound + "." + vib + ".");
            fw.Flush();
            fw.Close();
        }
    }
}